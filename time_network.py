from __future__ import print_function
import errno
import logging
import logging.handlers
import os
import subprocess
import sys
import time
import datetime
from statistics import mean, stdev

from tabulate import tabulate
import typer
import configparser
from pathlib import Path
from typing import Optional
from webdav3.client import Client

script_path = os.path.dirname(os.path.realpath(__file__))
log = logging.getLogger(__name__)
app = typer.Typer()
icommands_app = typer.Typer()
app.add_typer(icommands_app, name="icommands")
filesystem_app = typer.Typer()
app.add_typer(filesystem_app, name="filesystem")
webdav_app = typer.Typer()
app.add_typer(webdav_app, name="webdav")
config_dict = {}
config_webdav_dict = {}


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def run_cmd(cmd, run_env=False, unsafe_shell=False, check_rc=False):
    if not run_env:
        run_env = os.environ.copy()
    log.debug('run_env: {0}'.format(run_env))
    log.debug('running: {0}, unsafe_shell={1}, check_rc={2}'.format(cmd, unsafe_shell, check_rc))
    if unsafe_shell:
        start = time.perf_counter()
        p = subprocess.Popen(cmd, env=run_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    else:
        start = time.perf_counter()
        p = subprocess.Popen(cmd, env=run_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, err) = p.communicate()
    end = time.perf_counter()
    etime = end - start
    log.debug('  stdout: {0}'.format(out.decode("utf-8").strip()))
    log.debug('  stderr: {0}'.format(err.decode("utf-8").strip()))
    if check_rc:
        if p.returncode != 0:
            log.error(check_rc)
            sys.exit(p.returncode)
    return p.returncode, out.decode("utf-8").strip(), err.decode("utf-8").strip(), etime


def run_webdav(cmd, command):
    if isinstance(cmd[0], Client):
        client = cmd[0]
    else:
        log.error("Missing webdav client")
        sys.exit(1)
    if command.endswith("upload"):
        start = time.perf_counter()
        client.upload_sync(remote_path=cmd[2], local_path=cmd[1])
        end = time.perf_counter()
    elif command.endswith("download"):
        start = time.perf_counter()
        client.download_sync(remote_path=cmd[1], local_path=cmd[2])
        end = time.perf_counter()
    else:
        log.error("unknown webdav command: {}".format(command))
        sys.exit(1)
    etime = end - start
    return etime


def parse_configuration(conf):
    webdav_dict = {}
    default_dict = {}
    if conf is None:
        typer.echo("No config file")
        typer.echo("Try time_network.py --conf [FILE PATH]")
        raise typer.Abort()
    if conf.is_file():
        config = configparser.ConfigParser()
        config.read(conf)
        typer.echo("Config file defaults: {}".format(str(config.defaults())))
        if 'WEBDAV' in config:
            webdav_dict = {}
            typer.echo("Config file webdav:")
            for key in config['WEBDAV']:
                if key != "password":
                    typer.echo("{}: {}".format(key, config['WEBDAV'][key]))
                webdav_dict[key] = config['WEBDAV'][key]
        default_dict = config.defaults()
    elif conf.is_dir():
        typer.echo("Config is a directory")
        raise typer.Abort()
    elif not conf.exists():
        typer.echo("The configuration file doesn't exist")
        raise typer.Abort()

    return default_dict, webdav_dict


def data_transfer(cmd, command, avg_file, filesize):
    speed_arr = []
    num_threads = str(config_dict["num_threads"]) if int(filesize) > 32768 else "1"
    const_params = [command, "{:.3f}".format(filesize / 1048576), num_threads]
    # loop over the number of test per command
    for run in range(1, config_dict["runs_of_each"] + 1):
        log.info("Test number {}".format(run))
        # command execution
        # for webdav
        if command.startswith("webdav"):
            etime = run_webdav(cmd, command)
        # for icommands and filesystem commands
        else:
            ret_code, output, error, etime = run_cmd(cmd, check_rc=True)
            log.debug("{} stats: return code: {}, out: {}, err: {}".format(command, ret_code, output, error))
        log.debug("elapsed time: {}".format(etime))
        # computing performance: duration in seconds, bandwidth in Bit/s
        speed = (float(filesize / 1048576) / float(etime)) * 8
        log.info("Test completed, time: {:.3f} s - computed speed: {:.3f} MBit/s".format(etime, speed))
        line_per_run = const_params + [str(run), str(etime), str(speed)]

        speed_arr.append(speed)
        # writing the csv file with a line per each command execution
        with open(os.path.join(script_path, config_dict["results_file"]), 'a') as f:
            f.write((','.join(line_per_run)) + '\n')

    average_speed = mean(speed_arr)
    if config_dict["runs_of_each"] > 1:
        stddev_speed = stdev(speed_arr)
    else:
        stddev_speed = "N.A."
    line_per_size = const_params + [str(config_dict["runs_of_each"])] + [str(average_speed), str(stddev_speed)]
    # writing the CSV file with a line per file size
    with open(os.path.join(script_path, avg_file), 'a') as f:
        f.write(','.join(line_per_size) + '\n')

    return line_per_size


def init_output_files():
    extensions = "".join(Path(config_dict["results_file"]).suffixes)
    avg_file = str(config_dict["results_file"]).replace(extensions, '') + '_avg'
    ct = datetime.datetime.now()
    # setting of the csv headers in the output file for the results
    with open(os.path.join(script_path, config_dict["results_file"]), 'a') as f:
        f.write('------------[ ' + str(ct) + ' ]\n')
        f.write('command,MiB,N,run,seconds,speed\n')
    # setting of the csv headers in the output file for the average results
    with open(os.path.join(script_path, avg_file), 'a') as f:
        headers_line = 'cmd,size (MiB),#threads,#runs,avg_speed (Mbit/s),stdev'
        f.write('------------[ ' + str(ct) + ' ]\n')
        f.write(headers_line + '\n')

    return headers_line, avg_file


@icommands_app.command("upload")
def icommands_upload():
    # initialize the output files
    headers_line, avg_file = init_output_files()

    command = 'iput'
    run_cmd(['imkdir', '-p', config_dict["itargetdir"]], check_rc=True)
    ifilename = '{0}/N{1}_{2}'.format(config_dict["itargetdir"],
                                      config_dict["num_threads"],
                                      config_dict["suffix_file"])
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))
#        run_cmd(['truncate', '-s', '{0}M'.format(filesize), filename], check_rc=True)
        cmd = [command, '-f', '-N{0}'.format(config_dict["num_threads"]), filename, ifilename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)
        # cleaning the files created per each size
        os.unlink(filename)
        i = int(i + 1)

    # cleaning the whole target folder
    run_cmd(['irm', '-rf', config_dict["itargetdir"]], check_rc=True)
    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


@icommands_app.command("download")
def icommands_download():
    # initialize the output files
    headers_line, avg_file = init_output_files()

    command = 'iget'
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))
        # run_cmd(['truncate', '-s', '{0}M'.format(filesize), filename], check_rc=True)

        # upload a file to be able to download it later
        run_cmd(['iput', '-f', '-N{0}'.format(config_dict["num_threads"]), filename])
        os.unlink(filename)

        cmd = [command, '-f', '-N{0}'.format(config_dict["num_threads"]), filename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)

        # cleaning the files created per each size
        run_cmd(['irm', '-f', filename], check_rc=True)
        i = int(i + 1)

    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


@filesystem_app.command("upload")
def filesystem_upload():
    # initialize the output files
    headers_line, avg_file = init_output_files()

    command = 'cp'
    ifilename = '{0}/N{1}_{2}'.format(config_dict["itargetdir"],
                                      config_dict["num_threads"],
                                      config_dict["suffix_file"])
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))
        # run_cmd(['truncate', '-s', '{0}M'.format(filesize), filename], check_rc=True)
        cmd = [command, '-f', filename, ifilename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)
        # cleaning the files created per each size
        os.unlink(filename)
        i = int(i + 1)

    # cleaning the whole target folder
    run_cmd(['rm', '-rf', config_dict["itargetdir"]], check_rc=True)
    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


@filesystem_app.command("download")
def filesystem_download():
    # initialize the output files
    headers_line, avg_file = init_output_files()

    command = 'cp'
    run_cmd(['mkdir', '-p', config_dict["itargetdir"]], check_rc=True)
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))

        # upload a file to be able to download it later
        source_path = config_dict["itargetdir"] + os.sep + filename
        run_cmd(['cp', '-f', filename, source_path])
        os.unlink(filename)

        cmd = [command, '-f', source_path, filename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)

        # cleaning the files created per each size
        os.unlink(filename)
        run_cmd(['rm', '-f', source_path], check_rc=True)
        i = int(i + 1)

    # cleaning the whole source folder
    run_cmd(['rm', '-rf', config_dict["itargetdir"]], check_rc=True)
    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


def initialize_webdav():
    if (not config_webdav_dict) or len(config_webdav_dict) < 3:
        err_msg = "Expecting at least three parameters in the WEBDAV section of the conf file"
        log.error(err_msg)
        typer.echo(err_msg)
        typer.Exit(1)

    options = {
        'webdav_hostname': config_webdav_dict['url'],
        'webdav_login': config_webdav_dict['username'],
        'webdav_password': config_webdav_dict['password'],
        'chunk_size': 655360
    }
    client = Client(options)
    if 'ssl_verify' in config_webdav_dict:
        client.verify = config_webdav_dict['ssl_verify']
    if 'chunk_size' in config_webdav_dict:
        client.chunk_size = config_webdav_dict['chunk_size']
    if 'timeout' in config_webdav_dict:
        client.timeout = config_webdav_dict['timeout']

    return client


@webdav_app.command("upload")
def webdav_upload():
    # initialize the output files
    headers_line, avg_file = init_output_files()
    command = "webdav upload"
    client = initialize_webdav()

    if not client.check(config_dict["itargetdir"]):
        client.mkdir(config_dict["itargetdir"])
    ifilename = '{0}/N{1}_{2}'.format(config_dict["itargetdir"], 
                                      config_dict["num_threads"],
                                      config_dict["suffix_file"])
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))
        # run_cmd(['truncate', '-s', '{0}M'.format(filesize), filename], check_rc=True)
        cmd = [client, filename, ifilename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)
        # cleaning the files created per each size
        os.unlink(filename)
        i = int(i+1)

    # cleaning the whole target folder
    client.clean(config_dict["itargetdir"])
    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


@webdav_app.command("download")
def webdav_download():
    # initialize the output files
    headers_line, avg_file = init_output_files()
    command = "webdav download"
    client = initialize_webdav()

    if not client.check(config_dict["itargetdir"]):
        client.mkdir(config_dict["itargetdir"])
    data_table = list()
    i = 0
    # loop over file size range
    for filesize in config_dict["filesize_list"]:
        log.info("Testing with file size {} {}".format(filesize, config_dict["filesize_unit"]))
        filesize_bytes = (list(config_dict["filesize_list_bytes"])).pop(i)
        # the input file is created
        filename = '{0}{1}file_{2}'.format(filesize, config_dict["filesize_unit"], config_dict["suffix_file"])
        with open('%s' % filename, 'wb') as fout:
            fout.write(os.urandom(filesize_bytes))
        # run_cmd(['truncate', '-s', '{0}M'.format(filesize), filename], check_rc=True)
        # upload a file to be able to download it later
        source_path = config_dict["itargetdir"] + os.sep + filename
        client.upload_sync(remote_path=source_path, local_path=filename)
        os.unlink(filename)

        cmd = [client, source_path, filename]
        line = data_transfer(cmd, command, avg_file, filesize_bytes)
        data_table.append(line)
        # cleaning the files created per each size
        os.unlink(filename)
        i = int(i + 1)

    # cleaning the whole source folder
    client.clean(config_dict["itargetdir"])
    print(tabulate(data_table, headers=headers_line.split(','), tablefmt="simple"))


@app.callback(invoke_without_command=True)
def main(conf: Optional[Path] = typer.Option(None),
         result: Optional[Path] = typer.Option(None, "--file", "-f", help="result file path"),
         verbose: bool = typer.Option(False, "--verbose", "-v"),
         quiet: bool = typer.Option(False, "--quiet", "-q"),
         runs_of_each: int = typer.Option(None, "--runs", "-r"),
         itargetdir: str = typer.Option(None, "--target_dir"),
         num_threads: int = typer.Option(None, "-t"),
         filesize_list: str = typer.Option(None, "--size_list", "-s"),
         filesize_unit: str = typer.Option(None, "--size_unit", "-u")
         ):
    """
    run benchmark with values mentioned in config file
    or set the values with provided keys at command prompt
    """
    # Parse configuration file
    globals()['config_dict'], globals()['config_webdav_dict'] = parse_configuration(conf)
    # Override conf file with commandline options
    # number of time that each command is repeated
    config_dict["runs_of_each"] = int(runs_of_each if runs_of_each else config_dict["runs_of_each"])
    config_dict["itargetdir"] = itargetdir if itargetdir else config_dict["itargetdir"]
    # number of threads of each icommand
    config_dict["num_threads"] = int(num_threads if num_threads else config_dict["num_threads"])
    # range of file size passed as input (KB, MB, GB)
    config_dict["filesize_list"] = (filesize_list if filesize_list else config_dict["filesize_list"]).split(",")
    config_dict["filesize_unit"] = filesize_unit if filesize_unit else config_dict["filesize_unit"]
    config_dict["results_file"] = (result if result else config_dict["results_file"])
    # file source and file destination suffix randomized
    from datetime import datetime
    dt = datetime.now()
    ts = datetime.timestamp(dt)
    config_dict["suffix_file"] = int(ts)

    unit_dict = {'KB': 1024, 'MB': 1024 * 1024, 'GB': 1024 * 1024 * 1024}
    if config_dict["filesize_unit"] and config_dict["filesize_unit"] in unit_dict.keys():
        fs_list = []
        for filesize in config_dict["filesize_list"]:
            bytes_size = int(filesize)*int(unit_dict.get(config_dict["filesize_unit"]))
            fs_list.append(bytes_size)
        config_dict["filesize_list_bytes"] = fs_list
    else:
        typer.echo('Unknown file size unit: {}'.format(config_dict["filesize_unit"]))
        raise typer.Exit(0)

    if verbose & quiet:
        typer.echo('please choose either "verbose" or "quiet" ')
        raise typer.Exit(0)
    if verbose:
        log.setLevel(logging.DEBUG)
    elif quiet:
        log.setLevel(logging.WARNING)
    else:
        log.setLevel(logging.INFO)

    formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
    fh = logging.handlers.RotatingFileHandler('time_network.log', maxBytes=10000000, backupCount=5)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    log.addHandler(ch)


if __name__ == "__main__":
    app()
