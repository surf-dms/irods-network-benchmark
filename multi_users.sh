#!/bin/sh

for i in {1..10}
do
  python3 time_network.py --conf config1.ini -r 1000 --target_dir speedtest${i} --file resultfile${i}.txt webdav upload &
  sleep 1
done
