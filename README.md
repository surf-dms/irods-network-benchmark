# iRODS network benchmark 

This application aims to provide a way to test the upload/download performance and scalability of an iRODS server.  
It offers the possibility to use the icommands deployed in the local environment where the application runs. Or the 
webdav protocol. It includes also a file system option to test mounted folders. 

### Configuration

It requires the python modules
  - tabulate
  - typer
  - webdavclient3

Tested with python3.

Usage: time_network.py --help

See _config.ini.template_ about the input configuration parameters.

The results are written in csv format on a file whose path can be specified in the configuration file or as an input 
parameter.  
These are the csv fields:
  - **command**: command used for the test,
  - **MiB**: size of file ,
  - **N**: number of threads,
  - **run**: which iteration in uploading/downloading file,
  - **seconds**: time it took to run the command,
  - **speed**: time/ size of file

A second file with the same name of the one with the results is saved with suffix "_avg". That file contains the average
upload/download bandwidth in csv format with the following fields:  
  - **command**: command used for the test,
  - **MiB**: size of file,
  - **N**: number of threads,
  - **runs**: how many times the test was repeated,
  - **average_speed**: average bandwidth,
  - **speed_stdev**: standard deviation

A log file is also created with the name time_network.log 

### Example

Execute a test using the parameters in config.ini, but overloading the number of iterations (-r 1), the target folder 
(speedtest2) and the name of the result file (resultfile2). The test will upload files via webdav:   

`python3 time_network.py --conf config.ini -r 1 --target_dir speedtest2 --file resultfile2 webdav upload`

### Simulating concurrent clients

There is a simple shell script (multi_user.sh), which can execute multiple transfers at the same time, simulating 
requests from multiple clients. In order to really have multiple ongoing transfers it is necessary that each transfer 
lasts enough to allow the script to spawn a new process before the previous are completed.  
Be aware that the destination/source folder name is parameterized using the loop index. In case you want to start the 
script from multiple machines at the same time, you need to partition the space of the iterations, so that if one script's 
loop goes from 1 to 100, the other goes from 101 to 200.

### iRODS server side possible bottlenecks

#### DavRODS Web server
The Apache Web server reaches the limit of concurrent clients: you should see an error in the httpd logs, like:

`[mpm_prefork:error] [pid 21427] AH00161: server reached MaxRequestWorkers setting, consider raising the MaxRequestWorkers setting`

you can increase the number of accepted concurrent clients with the parameter _MaxRequestWorkers_ in httpd.conf or in 
the specific mpm_prefork module configuration (default 256).

#### iCAT server
The iCAT Postgresql server reaches the limit of concurrent connections: you should see an error in the iCAT logs:

`ERROR: cllConnect: [unixODBC]FATAL: remaining connection slots are reserved for non-replication superuser connections`

you can increase that limit through the parameter _max_connections_ in postgresql.conf (default = 100 + 15 reserved).

### WebDAV clients

Operations logged on the Apache log for each upload done using 
 - the native Win10 WebDAV client:  
```
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "PROPFIND /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 404 196 "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "PUT /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 201 239 "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "LOCK /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 200 446 "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "PROPPATCH /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 207 1182 "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - - [26/Aug/2022:08:56:58 +0000] "HEAD /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 401 - "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "HEAD /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 200 - "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "PUT /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 204 - "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "PROPPATCH /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 207 712 "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
xx.yy.xx.yy - user1 [26/Aug/2022:08:56:58 +0000] "UNLOCK /my/beautiful/dataset/testfile_n_10.txt HTTP/2.0" 204 - "-" "Microsoft-WebDAV-MiniRedir/10.0.19044"  
```
 - the Cyberduck client on Win10:  
```
xx.yy.xx.yy - user1 [26/Aug/2022:08:28:52 +0000] "PUT /my/beautiful/dataset/testfile_n_10.txt HTTP/1.1" 201 245 "-" "Cyberduck/8.2.3.36880 (Windows 10/10.0
) (amd64)"
```
 - the Mountainduck CLI on Linux:  
```
xx.yy.xx.yy - user1 [26/Aug/2022:11:15:57 +0000] "PUT /10x5smallfiles/testdir5/file007.bin HTTP/1.1" 201 208 "-" "Cyberduck/8.4.2.38090 (Linux/5.18.17-100.fc35.x86_64) (amd64)"
```
 - the File Centipede client on Linux:  
```
xx.yy.xx.yy - user1 [29/Aug/2022:12:11:11 +0000] "PUT /10000x1KBfiles/file6891.bin HTTP/1.1" 201 200 "-" "Mozilla/5.0 (Linux x86_64) Firefox/78.0"
```
 - this python script uploading via webdav protocol:  
```
xx.yy.xx.yy - user1 [26/Aug/2022:10:55:37 +0000] "HEAD /speedtest2/ HTTP/1.1" 200 - "-" "python-requests/2.27.1"  
xx.yy.xx.yy - user1 [26/Aug/2022:10:55:37 +0000] "PUT /speedtest2/N1_1661511336 HTTP/1.1" 201 197 "-" "python-requests/2.27.1"
```
